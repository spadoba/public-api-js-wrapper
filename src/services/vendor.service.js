import RequestService from './@request.service';
import { API_URL } from '../config';

class VendorService {

  /**
   * POST https://api.spadoba.com/{version}/vendor-public/auth/sms-send-code
   * 
   * Статус 201. Вернет пустое тело ответа.
   * 
   * @param {string} phone_number - телефонный номер владельца бизнеса в международном формате (например: 37529xxxxxxx)
   */
  sendSmsCode (phone_number) {
    const url = `${API_URL}/auth/sms-send-code`;
    const data = { phone_number };

    return RequestService.postRequest(url, data)
      .then((resp) => {
        if (resp.status === 404) {
          throw new Error(`Владелец магазина с телефонным номером ${data.phone_number} не найден`)
        } 
        if (resp.status === 400) {
          throw new Error(`Передан неверный формат данных`)
        }

        return resp;
      });
  }

  /**
   * POST https://api.spadoba.com/{version}/vendor-public/auth/verify
   * 
   * Статус - 200. При успешной проверке получаем авторизационный токен (auth_token).
   * 
   * @param {string} phone_number - телефонный номер владельца бизнеса в международном формате (например: 37529xxxxxxx)
   * @param {string} code - Код подтверждения (6 цифр без пробелов и других разделителей)
   */
  verifyVendor (phone_number, code) {
    const url = `${API_URL}/auth/verify`;
    const data = {
      phone_number,
      code
    };

    return RequestService.postRequest(url, data)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Неверный формат переданных данных');
        }
        if (resp.status === 404) {
          throw new Error(`Код подтверждения ${code} для телефона ${phone_number} не совпадает`);
        }
        return resp.json();
      });
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * GET https://api.spadoba.com/{version}/vendor-public/vendor
   * 
   * Статус 200. Получаем информацию о магазине.
   */
  getVendorData () {
    const url = `${API_URL}/vendor`;
    
    return RequestService.getRequest(url, authToken)
      .then((resp) => resp.json());
  }

}

export default new VendorService();
