class RequestService {

  async getRequest (url, token = null) {
    const options = {
      method: 'GET',
      headers: this.createHeaders(token)
    };

    try {
      const data = await fetch(url, options)
        .then(resp => resp.json());

      return data;
    } catch (error) {
      throw new Error(error);
    }
  }

  async postRequest (url, data, token = null) {
    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: this.createHeaders(token)
    };

    try {
      const resp = await fetch(url, options)
        .then((resp) => resp)

      return resp;
    } catch (error) {
      throw new Error(error);
    }
  }

  async putRequest (url, data, token = null) {
    const options = {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: this.createHeaders(token)
    };

    try {
      const resp = await fetch(url, data)
        .then((resp) => resp);
    } catch (error) {
      throw new Error(error);
    }
  }

  async deleteRequest (url, token) {
    const options = {
      method: 'DELETE',
      headers: this.createHeaders(token)
    };

    try {
      const resp = await fetch(url)
        .then((resp) => resp);
    } catch (error) {
      throw new Error(error);
    }
  }

  createHeaders (token) {
    return {
      'Content-Type': 'application/json',
      'Authorization': token
    };
  }

}

export default new RequestService();
