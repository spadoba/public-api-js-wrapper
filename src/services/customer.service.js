import RequestService from './@request.service';
import { API_URL } from '../config';

class CustomerService {

  /**
   * ! Обязательно наличие авторизационного токена
   * GET https://api.spadoba.com/v1.0/vendor-public/customers/{customerId}
   * 
   * Статус 200. Вернет информацию о покупателе
   * 
   * @param {string} customerId - id покупателя
   */
  getCustomerData (customerId) {
    const url = `${API_URL}/customers/${customerId}`;
    return RequestService.getRequest(url, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Неверный формат переданных данных');
        }
        if (resp.status === 404) {
          throw new Error('Покупатель не найден');
        }
        return resp.json();
      });
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * POST https://api.spadoba.com/{version}/vendor-public/customers/sms-send-code
   * 
   * Статус 200. Вернет пустое тело ответа. Покупателю будет выслан смс-код. С помощью этого запроса можно, например, подписать нового покупателя на магазин (см. subscribeCustomer()), если у него не установлено мобильное приложение spadoba. Либо получить иформацию по уже подписанному покупателю (если он, к примеру, забыл телефон дома)
   * 
   * @param {string} phone_number - телефонный номер покупателя в международном формате (например: 37529xxxxxxx)
   */
  sendSmsCode (phone_number) {
    const url = `${API_URL}/customers/sms-send-code`;
    const data = { phone_number };

    return RequestService.postRequest(url, data, authToken);
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * POST https://api.spadoba.com/{version}/vendor-public/customers/subscribe
   * 
   * Статус 200. Вернет id покупателя, подпишет покупателя на магазин. В смс, которое будет выслано покупателю будет также содержаться ссылка на скачивание мобильного приложения spadoba.
   * 
   * @param {string} phone_number - телефонный номер покупателя в международном формате (например: 37529xxxxxxx)
   * @param {string} code - Код подтверждения (6 цифр без пробелов и других разделителей)
   */
  subscribeCustomer (phone_number, code) {
    const url = `${API_URL}/customers/subscribe`;
    const data = {
      phone_number,
      code
    };

    return RequestService.postRequest(url, data, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Неверный формат переданных данных');
        }
        if (resp.status === 404) {
          throw new Error(`Телефонный номер ${phone_number} не найден либо не существует`);
        }
        return resp.json();
      });
  }

}

export default new CustomerService();
