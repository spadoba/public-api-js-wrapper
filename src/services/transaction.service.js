import RequestService from './@request.service';
import { API_URL } from '../config';

class TransactionService {

  /**
   * ! Обязательно наличие авторизационного токена
   * POST https://api.spadoba.com/{version}/vendor-public/purchase-transactions
   * 
   * Статус 200. Будет открыта новая транзакция для совершения покупки с доступной для покупателя скидочной программой в данном мазазине. Вернет id покупателя и id транзакции. Время существования открытой, но не подтвержденной магазином транзакции - 40 минут. Id транзакции потребуется для рассчета покупки для покупателя с учетом его скидок и проч. (см. prepareTransaction())
   * 
   * @param {string} phone_number - телефонный номер покупателя в международном формате (например: 37529xxxxxxx)
   * @param {string} code - Код подтверждения (6 цифр без пробелов и других разделителей
   */
  openCustomerTransaction (phone_number, code) {
    const url = `${API_URL}/purchase-transactions`;
    const data = {
      phone_number,
      code
    };

    return RequestService.postRequest(url, data, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error(`Возможно код ${code} устарел, либо данные переданы неверно`);
        }
        if (resp.status === 404) {
          throw new Error(`Покупатель с телефонным номером ${phone_number} не найден`);
        }

        return resp.json();
      });
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * PUT https://api.spadoba.com/{version}/vendor-public/purchase-transactions/{transactionId}/purchase
   * 
   * Статус 200. Покупка успешно размещена в транзакции. Если покупка добавлена, но не одобрена (см. approveTransaction()), срок действия транзакции ограничен 24 часами с момента создания. Захолдированные, но не использованные бонусы становятся доступны снова после удаления транзакции (см. deleteTransaction())
   * 
   * @param {string} external_user_id - id продавца который обслуживает покупателя.
   * @param {number} purchase_value - Полная сумма покупки
   * @param {string} transactionId - id транзакции (мы получили его с помощью метода openCustomerTransaction())
   * @param {object} extra - Дополнительные параметры покупки. Если ничего не передано, то расчет по умолчанию производится исходя из наилучших доступных для покупателя условий совершения покупки.
   */
  prepareTransaction (external_user_id, purchase_value, transactionId, extra = {}) {
    const url = `${API_URL}/purchase-transactions/${transactionId}/purchase`;
    const data = {
      external_user_id,
      purchase_value,
      extra
    };

    return RequestService.putRequest(url, data, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Не удалось разместить покупку в транцакции');
        }
        if (resp.status === 4002) {
          throw new Error('Истекло время размещения покупки в транзакции, попробуйте еще раз ввести код покупателя');
        }
        if (resp.status === 404) {
          throw new Error('Транзакция не найдена');
        }

        return resp.json();
      }); 
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * PUT https://api.spadoba.com/{version}/vendor-public/purchase-transactions/{transactionId}/approve
   * 
   * Статус 200. Покупка подтверждена. Скидки применены. Информация о совершенной покупке занесена в систему Spadoba.
   * 
   * @param {string} transactionId 
   */
  approveTransaction (transactionId) {
    const url = `${API_URL}/purchase-transactions/${transactionId}/approve`;

    return RequestService.putRequest(url, null, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Попробуйте повторить');
        }
        if (resp.status === 404) {
          throw new Error('Транзакция не найдена');
        }
        return resp.json();
      });
  }

  /**
   * ! Обязательно наличие авторизационного токена
   * DELETE https://api.spadoba.com/{version}/vendor-public/purchase-transactions/{transactionId}
   * 
   * Статус 204. Вернет пустое тело ответа. Транзакция удалена.
   * 
   * @param {string} transaction_id 
   */
  deleteTransaction (transaction_id) {
    const url = `${API_URL}/purchase-transactions/${transactionId}`;
    
    return RequestService.deleteRequest(url, authToken)
      .then((resp) => {
        if (resp.status === 400) {
          throw new Error('Неверные переданные данные');
        }
        if (resp.status === 404) {
          throw new Error('Транзакция не найдена');
        }
        return resp;
      });
  }

}

export default new TransactionService();