# Public Spadoba-API service example

> Ссылка на полную документацию <https://api-docs.spadoba.com>

> Ссылка на демонстрационный пример <https://api-sandbox.spadoba.com/>

В папке *services* находятся пример работы с Public Spadoba API. Там же (в папке *response-examples*) лежат json примеры ответов сервера (каждый файл назван "в честь" соотв. метода)

К каждому методу также добавлены подробные комментарии.